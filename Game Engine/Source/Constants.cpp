// ======================================= //
// Melvin the Marvellous Monster from Mars //
//                                         //
// Author: Kevin Scroggins                 //
// E-Mail: nitro404@hotmail.com            //
// Date: April 11, 2010                    //
// ======================================= //

#include "Constants.h"

const double Constants::GRAVITY = 9.81;
const double Constants::PI = 3.141592653589793;
const int Constants::GRID_SIZE = 64;
